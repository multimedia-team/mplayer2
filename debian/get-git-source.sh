#!/bin/sh

set -e

PACKAGE=mplayer2
BASE_REL=$(dpkg-parsechangelog 2>/dev/null | sed -ne 's/Version: \([0-9]\)\+.*/\1/p')
OLDDIR=${PWD}
GOS_DIR=${OLDDIR}/get-orig-source
GIT_DESCRIBE_STR='git describe | sed -e "s/v\(.*\)/\1/"'

if [ -z ${BASE_REL} ]; then
	echo 'Please run this script from the sources root directory.'
	exit 1
fi


rm -rf ${GOS_DIR}
mkdir ${GOS_DIR} && cd ${GOS_DIR}
git clone git://git.mplayer2.org/mplayer2.git ${PACKAGE}
cd ${PACKAGE}/
GIT_DESCRIBE=$(eval "${GIT_DESCRIBE_STR}")
cd .. && XZ_OPT=-f9 tar cJf \
	${OLDDIR}/${PACKAGE}_${GIT_DESCRIBE}.orig.tar.xz \
	${PACKAGE} --exclude-vcs
rm -rf ${GOS_DIR}
